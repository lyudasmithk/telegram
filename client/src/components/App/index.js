import LoginForm from '../LoginForm';
import SubmitForm from '../SubmitForm';
import styles from './stylesheet.css';
import User from '../User';

class App {
  constructor({ user }) {
    this.element = document.createElement('div');
    this.element.classList.add(styles.container);
    this.isLoginForm = true;
    this.data = {};
    this.user = user;
    this.render(this.isLoginForm);
    return this.element;
  }

  handleLogin = (data) => {
    this.loginData = data;
    this.isLoginForm = false;
    this.render();
  };

  handleCodeSubmit = (userData) => {
    this.user = userData;

    this.render();
  };

  render = () => {
    this.element.innerHTML = '';
    if (this.user) {
      this.element.appendChild(new User(this.user));
    } else {
      this.isLoginForm ?
        this.element.appendChild(new LoginForm({
          handleLogin: this.handleLogin,
          isLoginForm: this.isLoginForm,
          data: this.data
        })) :
        this.element.appendChild(new SubmitForm({
          data: this.data,
          handleSubmit: this.handleCodeSubmit,
          user: this.user
        }));
    }
  }
}

export default App;

