import style from './stylesheet.css';

class DialogItem {
  constructor(params) {
    this.element = document.createElement('div');
    this.params = params;
    this.render();

    return this.element;
  }

  render() {
    this.element.innerHTML = `
      <div class=${style.item}>
      <p><b>title</b> - ${this.params.user}</p>
        <p><b>top message</b> - ${this.params.top_message}</p>
      </div>
    `;
  }
}

export default DialogItem;
