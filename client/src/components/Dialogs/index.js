import style from './stylesheet.css';
import DialogItem from '../DialogItem';
import DialogText from '../DialogText';

class Dialogs {
  constructor(dialogs) {
    this.element = document.createElement('div');
    this.element.classList.add(style.mainComtainer);
    this.dialogs = dialogs;
    this.render();

    return this.element;
  }

  render() {
    this.element.innerHTML = '';
    const dialogsContainer = document.createElement('div');
    dialogsContainer.classList.add(style.dialogsContainer);
    this.dialogs.forEach((item) => {
      dialogsContainer.appendChild(new DialogItem(item));
    });
    this.element.appendChild(dialogsContainer);
    this.element.appendChild(new DialogText());
  }
}
export default Dialogs;
