import styles from './stylesheet.css';

class LoginForm {
  constructor({ handleLogin, isLoginForm, data }) {
    this.handleLogin = handleLogin;
    this.isLoginForm = isLoginForm;
    this.data = data;
    this.element = document.createElement('form');
    this.element.classList.add(styles.formContainer);
    this.element.addEventListener('submit', this.submitHadler);
    this.render();

    return this.element;
  }
  render() {
    this.element.innerHTML = `
      <h3 class="centered">Sign in</h3>
      <p class="centered">Enter your full phone number.</p>
      <input id="input" placeholder="phone number">
      <button id="submitButt">Next ></button>
    `;
  }
  submitHadler = (event) => {
    event.preventDefault();
    const value = document.getElementById('input').value;
    this.data.phone = value;
    fetch('/api/auth/send-code', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        phone: value
      })
    }).then((resp) => {
      if (resp.status === 200) {
        this.isLoginForm = false;
        resp.json().then((data) => {
          this.data.phone_code_hash = data.phone_code_hash;
          console.log(this.data);
          this.handleLogin();
        });
      }
    });
    // /const xhr = new XMLHttpRequest();
    // xhr.open('POST', '/api/auth/send-code', true);
    // xhr.setRequestHeader('Content-Type', 'application/json');
    // xhr.onreadystatechange = (resp) => {
  }
}

export default LoginForm;
