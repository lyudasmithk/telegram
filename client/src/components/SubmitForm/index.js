
class SubmitForm {
  constructor({ data, handleSubmit, user }) {
    this.element = document.createElement('form');
    this.data = data;
    this.user = user;
    this.handleSubmit = handleSubmit;
    this.render();
    this.element.addEventListener('submit', this.signInHandler);
    return this.element;
  }

  signInHandler = (event) => {
    event.preventDefault();
    const value = document.getElementById('code').value;
    this.data.code = value;
    fetch('/api/auth/sign-in', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        phone: this.data.phone,
        phone_code_hash: this.data.phone_code_hash,
        code: this.data.code
      })
    }).then((resp) => {
      if (resp.ok) {
        resp.json()
        .then((userData) => {
          this.user = userData.user;
          this.handleSubmit(this.user);
        });
      }
    })
      .catch(error => console.log(error));
  }

  render() {
    this.element.innerHTML = `
      <h3>Submit your phone number</h3>
      <label>Enter your code</label>
      <input type="text" id="code">
      <button type="submit">Next ></button>
    `;
  }
}

export default SubmitForm;

