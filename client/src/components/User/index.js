import Dialogs from '../Dialogs';
import style from './stylesheet.css';

class User {
  constructor(data) {
    this.element = document.createElement('div');
    this.element.classList.add(style.userContainer);
    this.data = data;
    this.dialogs = [];

    fetch('/api/dialogs')
      .then(response => response.json())
      .then((responseData) => {
        this.dialogs = responseData.dialogs;
        this.users = responseData.users;
        this.messages = responseData.messages;
        this.chats = responseData.chats;
        console.log(this.chats);
        //this.state = {dialogs, users, messages}

        this.render();
      });

    return this.element;
  }

  render() {
      const dialogs = this.dialogs.map((dialog) => {
        const topMessage = this.messages.find(msg => msg.id === dialog.top_message);
        let user;
        if (dialog.peer.user_id) {
           user = this.users.find((usr) => {
            return usr.id === dialog.peer.user_id;
          });
        }
        else {
          user = this.chats.find((chat) => {
            return chat.id === (dialog.peer.channel_id || dialog.peer.chat_id);
          });
        }
        return {
          ...dialog,
          top_message: topMessage.message,
          user: user.first_name || user.title
        };
      });


      // const dialogs = getExtendedDialogs({
      //     dialogs: this.dialogs,
      //     messages: this.messages,
      //   });

    this.element.innerHTML = `
      <div>
        <p>${this.data.first_name} ${this.data.last_name}</p>
      </div>
    `;
    // for (let i = 0; i < this.dialogs.length; i++) {
    //   const p = document.createElement('p');
    //   p.innerHTML = JSON.stringify(this.dialogs[i]);
    //   this.element.appendChild(p);
    // }
    this.element.appendChild(new Dialogs(dialogs));
  }
}
export default User;



// function getExtendedDialogs(state) { // selector function
//   const {dialogs} = state;
//
//   return dialogs.map((dialog) => {
//     const topMessage = getMessageById(state);
//     return {
//       ...dialog,
//       top_message: topMessage.message,
//     };
//   });
// }
//
// function getMessageById(state) { // selector function
//   const {messages} = state;
//
//   return messages.find(msg => msg.id === dialog.top_message);
// }