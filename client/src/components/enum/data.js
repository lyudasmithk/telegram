const data = { _: 'user',
  flags: 1143,
  self: true,
  id: 254463345,
  access_hash: '17424728060254185474',
  first_name: 'Lyuda',
  last_name: 'Kovalchuk',
  phone: '380955077626',
  photo:
  { _: 'userProfilePhoto',
    photo_id: '1092911745262004137',
    photo_small: [Object],
    photo_big: [Object] },
  status: { _: 'userStatusOffline', was_online: 1512563535 } };

export default data;
