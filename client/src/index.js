import 'normalize.css';
import './styles.css';
import App from './components/App';

fetch('/api/user')
  .then(res => res.json())
  .then(({ user }) => document.getElementById('root').appendChild(new App({ user })))
  .catch((err) => {
    console.log(err);
  });
