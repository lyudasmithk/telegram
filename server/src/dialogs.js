const telegram = require('./init');

module.exports = {
  getDialogs: (req) => {
    return telegram('messages.getDialogs', {
      limit: 50
    })
  }
};
