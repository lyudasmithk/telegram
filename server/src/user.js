module.exports = {
  getUser: () => {
    if (!global.user) { return Promise.reject({ code: 403, description: 'Unauthorized' }); }
    return Promise.resolve(global.user);
  }
};
